import time
import sched
import subprocess
import sys
import json

def runcmd(cmd, err_f=None, out_f=None):
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outs, errs = proc.communicate()
    if out_f:
        with open(out_f, 'w') as of:
            of.write(outs.decode('utf-8'))
    if err_f:
        with open(err_f, 'w') as ef:
            ef.write(errs.decode('utf-8'))

def sched_cmd(delay, out_file, err_file, cmd):
    print(cmd)
    s = sched.scheduler(time.time, time.sleep)
    s.enter(int(delay), 1, runcmd, argument=(cmd,), kwargs={'err_f': err_file, 'out_f': out_file})
    s.run()

if __name__ == '__main__' :
   sched_cmd(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4:]) 

