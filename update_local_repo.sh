#!/bin/bash

LOCAL_SRC=$1
REPO_URL=$2
VERSION=$3
update_local(){
    auxd=`mktemp -d`
    git -C $auxd clone -b $VERSION $REPO_URL
    mv $LOCAL_SRC /tmp/backup
    mkdir -p $LOCAL_SRC
#    git -C $LOCAL_SRC clone -b $VERSION $REPO_URL
    echo "mv -T $auxd/* $LOCAL_SRC"
    exit
}

eval update_local
