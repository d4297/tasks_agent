#!/bin/bash


run_workloads(){
    wl_sh=$1
    outs_dir=$2
    version_file=$3
    stdout_file=$4
    stderr_file=$5
    versions=( "${@:6}" )
    for v in ${versions[@]}
    do
        echo $v
        mkdir -p $outs_dir/$v
        echo $v > $outs_dir/$v/$version_file
        python$v $wl_sh > $outs_dir/$v/$stdout_file 2&> $outs_dir/$v/$stderr_file
    done
        
}


repo_differs(){
    version=$1
    local_src=$2
    test "`git -C $local_src diff $version --relative`" == "" && echo 0 || echo 1
}

delete_and_clone(){
    update_local=$1
    repo_url=$2
    version=$3
    local_src=$4
    eval $update_local $local_src $repo_url $version
    exit
}

send_results(){
    collector_type=$1
    collector_host=$2
    artifacts_dir=$3
    version_file=$4
    stdout_file=$5
    stderr_file=$6
    versions=$7
    test $collector_type == "etcd" && eval send_2_etcd $collector_host $artifacts_dir $versions_file $stdout_file $sderr_file $versions

}

send_2_etcd(){
    etcd_host=$1:2379
    artifacts_dir=$2
    version_file=$3
    stdout_file=$4
    stderr_file=$5
    versions=$6
    for v in $versions
    do
        curl http://$etcd_host/v2/keys/versions/$hostname/$v/version -d value="`cat $artifacts_dir/$v/$versions_file`"
        curl http://$etcd_host/v2/keys/stderr/$hostname/$v/stderr -d value="`cat $artifacts_dir/$v/$stderr_file`"
        curl http://$etcd_host/v2/keys/stdout/$hostname/$v/stdout -d value="`cat $artifacts_dir/$v/$stdout_file`"
    done
}

PARSER=$1
CFG=$2

main(){

    PARSER=`python3 $PARSER $CFG | jq -r '.src.parser'`
    CFG=`python3 $PARSER $CFG | jq -r '.cfg'`
    SCHEDULER=`python3 $PARSER $CFG | jq -r '.src.scheduler'`
    GIT_REPO=`python3 $PARSER $CFG | jq -r '.repo.url'`
    GIT_VERSION=`python3 $PARSER $CFG | jq -r '.repo.version'`
    SRC_PATH=`python3 $PARSER $CFG | jq -r '.src.path'`
    WL_SH=`python3 $PARSER $CFG | jq -r '.src.workload'`
    UPDATE_LOCAL=`python3 $PARSER $CFG | jq -r '.src.update_local'`
    PERIOD=`python3 $PARSER $CFG | jq -r '.period'`
    OUTS_DIR=`python3 $PARSER $CFG | jq -r '.outs.path'`
    VER_FNAME=`python3 $PARSER $CFG | jq -r '.outs.version'`
    OUT_FNAME=`python3 $PARSER $CFG | jq -r '.outs.stdout'`
    ERR_FNAME=`python3 $PARSER $CFG | jq -r '.outs.stderr'`
    PY_VERSIONS=`python3 $PARSER $CFG | jq -r '.python_versions[]'`
    COLLERCTOR_TYPE=`python3 $PARSER $CFG | jq -r '.collector.type'`
    COLLERCTOR_HOST=`python3 $PARSER $CFG | jq -r '.collector.host'`
    PYVS=( )
    for pyv in $PY_VERSIONS
    do
        PYVS+=( "$pyv" )

    done
    test 1 -eq "`repo_differs $GIT_VERSION $SRC_PATH`" &&  `delete_and_clone $SRC_PATH/$UPDATE_LOCAL $GIT_REPO $GIT_VERSION $SRC_PATH` || echo nodiffers 
    eval run_workloads $SRC_PATH/WL_SH $OUTS_DIR $VER_FNAME $OUT_FNAME $ERR_FNAME ${PYVS[@]}
    eval send_results $COLLECTOR_TYPE $COLLECTOR_HOST $OUTS_DIR $VER_FNAME $OUT_FNAME $ERR_VNAME $PY_VERSIONS
    eval python3 $SCHEDULER $PERIOD $OUTS_DIR/sched.out $OUTS_DIR/sched.err "$0 $PARSER $CFG"

}

eval main

