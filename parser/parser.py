import sys
import yaml
import json

with open(sys.argv[1], 'r') as f:
    settings = yaml.load(f, Loader=yaml.FullLoader)

print(json.dumps(settings))
